let express = require('express')
const path = require('path')
let app = express()
const port = process.env.PORT || 8092

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'))

// Routes
app.get('/', (request, response) => {
    response.render('index')
})

app.get('/creerSession', (request, response) => {
    response.render(path.join(__dirname + '/views/creerSession.ejs'))
})

app.get('/sessionEnCours', (request, response) => {
    response.render(path.join(__dirname + '/views/sessionEnCours.ejs'))
})

// Port d'écoute
app.listen(port, function (){
    console.log('App running on port ' + port)
})

// Commentaire
