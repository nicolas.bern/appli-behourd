module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 13,
    sourceType: 'module',
  },
  plugins: [
    'vue',
  ],
  rules: {
    "indent": ["off", 2],
    "linebreak-style": ["off", "unix"],
    "lines-between-class-members": ["off", "always"],
    "semi": ["off", "always"],
    "space-infix-ops": ["off", "always"],
    "no-multi-assign": ["off", "always"],
    "dot-notation": ["off", "always"],
    "quotes": ["off", "always"],
    "prefer-arrow-callback": ["off", "always"],
    "no-multiple-empty-lines": ["off", "always"],
    "no-unused-vars": ["off", "always"],
    "no-else-return": ["off", "always"],
    "padded-blocks": ["off", "always"]
  },
};
