const assert = require('assert')
const sinon = require('sinon')
let Combattant = require('../app/Combattant.js')
// Given 2 players WHEN session start THEN Launch a game in 1V1
describe('Création combattant', function()
{
    it('Créer un combattant', function(){
        let combattant = new Combattant("GRANS","Paul",134,1,2006);

        // Vérifie que le combattant est bien créé
        var test = Object.keys(combattant).length !== 0;
        assert.ok(test);
    })
})

